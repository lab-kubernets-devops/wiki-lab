# Sobre

Esta é um lab voltado para o aprendizado sobre kubernetes

# kubernetes

## Sobre
O Kubernetes é uma engine de orquestração de contêineres Open Source utilizado para automatizar a implantação, dimensionamento e gerenciamento de aplicativos em contêiner. 

## História
Originalmente, o Kubernetes foi criado e desenvolvido pelos engenheiros do Google. O Google foi um dos pioneiros no desenvolvimento da tecnologia de containers Linux. Além disso, a empresa já revelou publicamente que tudo no Google é executado em containers (inclusive, essa é a tecnologia por trás dos serviços em cloud da empresa). 

## Intalação
[Documentação oficial](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/) com as instruções e pré-requisitos para a intalação.


## Overview
### Kuberntes cluster
![image-overview-cluster](https://d33wubrfki0l68.cloudfront.net/152c845f25df8e69dd24dd7b0836a289747e258a/4a1d2/docs/tutorials/kubernetes-basics/public/images/module_02_first_app.svg)
### Kubernetes overview
![image-overview](http://missaodevops.com.br/img/warrior/m06-10-kubernetes-resources-overview.png)

### Namespaces
É a segregação lógica de nosso cluster, permitem criar divisões de 'ambientes' dentro do nosso cluster.
O que nos possibilita, por exemplo, ter N ambientes lógicos como Desenvolvimento, Homologação e Produção, ou seja, são fatias de nosso cluster físico.

### Deployment

Deployments são uma das cargas de trabalho mais comuns para se criar e gerenciar diretamente. Os deployments usam os replication sets como blocos construtivos, adicionando a funcionalidade de gerenciamento flexível do ciclo de vida ao mix.

### ReplicaSet
Solicitado o Deploy, o Kubernetes imediatamente cria um Replica Set, que é o recurso responsável por monitorar e manter a quantidade de execuções(pod) por nós informadas.

### POD
POD é onde estão executando nossos containers (sim, um POD, pode ter mais de um container). Por 'viverem' juntos, é garantido que esses containers compartilhem os mesmos recursos.

### Services
Uma maneira abstrata de expor um aplicativo em execução em um conjunto de Pods como um serviço de rede.
Com o Kubernetes, você não precisa modificar seu aplicativo para usar um mecanismo de descoberta de serviço desconhecido. O Kubernetes fornece aos pods seus próprios endereços IP e um único nome DNS para um conjunto de pods e pode balancear a carga entre eles.


### Ingress
A entrada expõe as rotas HTTP e HTTPS de fora do cluster para serviços dentro do cluster. O roteamento de tráfego é controlado por regras definidas no recurso Ingress.
Um Ingress pode ser configurado para fornecer aos Serviços URLs acessíveis externamente, tráfego de equilíbrio de carga, encerramento de SSL / TLS e oferta de hospedagem virtual baseada em nome. [Um controlador](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) de entrada é responsável por cumprir a entrada, geralmente com um balanceador de carga, embora também possa configurar seu roteador de borda ou front-ends adicionais para ajudar a lidar com o tráfego.


## Referências
[http://missaodevops.com.br/docs/warrior_orquestracao_kubernetes.html](http://missaodevops.com.br/docs/warrior_orquestracao_kubernetes.html)

[https://www.redhat.com/pt-br/topics/containers/what-is-kubernetes](https://www.redhat.com/pt-br/topics/containers/what-is-kubernetes)

[https://kubernetes.io/docs/home/](https://kubernetes.io/docs/home/)

[https://www.digitalocean.com/community/tutorials/uma-introducao-ao-kubernetes-pt](https://www.digitalocean.com/community/tutorials/uma-introducao-ao-kubernetes-pt)
